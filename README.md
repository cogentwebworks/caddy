## very small caddy container but still have a working s6-overlay process and socklog-overlay


[![Docker Pulls](https://img.shields.io/docker/pulls/cogentwebs/caddy.svg)](https://hub.docker.com/r/cogentwebs/caddy/)
[![Docker Stars](https://img.shields.io/docker/stars/cogentwebs/caddy.svg)](https://hub.docker.com/r/cogentwebs/caddy/)
[![Docker Build](https://img.shields.io/docker/automated/cogentwebs/caddy.svg)](https://hub.docker.com/r/cogentwebs/caddy/)
[![Docker Build Status](https://img.shields.io/docker/build/cogentwebs/caddy.svg)](https://hub.docker.com/r/cogentwebs/caddy/)

This is a very small caddy container but still have a working s6-overlay process and socklog-overlay .